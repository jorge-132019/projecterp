import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';

import { HomeComponent } from './pages/home/home.component';
import { CargarScriptsService } from './cargar-scripts.service';
import { InicioSesionComponent } from './pages/inicio-sesion/inicio-sesion.component';
import { RegistroCuentaComponent } from './pages/registro-cuenta/registro-cuenta.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FooterComponent } from './pages/footer/footer.component';
import { HeaderComponent } from './pages/header/header.component';
import { SideBarComponent } from './pages/side-bar/side-bar.component';
import { FormTributosComponent } from './pages/form-tributos/form-tributos.component';
import { FormComprasComponent } from './pages/form-compras/form-compras.component';
import { ViewFileComponent } from './pages/view-file/view-file.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ProfileComponent } from './pages/profile/profile.component';
import { authControl } from './providers/authControl';
import { ViewComprasComponent } from './pages/view-compras/view-compras.component';
import { FormContratoComponent } from './pages/form-contrato/form-contrato.component';
import { ViewContratoComponent } from './pages/view-contrato/view-contrato.component';
import { NotificationService } from './providers/notification.service';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { HttpClientModule  } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InicioSesionComponent,
    RegistroCuentaComponent,
    DashboardComponent,
    FooterComponent,
    HeaderComponent,
    SideBarComponent,
    FormTributosComponent,
    FormComprasComponent,
    ViewFileComponent,
    ProfileComponent,
    ViewComprasComponent,
    FormContratoComponent,
    ViewContratoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule, // para integrar formulario reactivo
    PdfViewerModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [
    CargarScriptsService,
    authControl,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
