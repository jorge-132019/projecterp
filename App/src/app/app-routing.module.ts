import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComprasComponent } from './pages/form-compras/form-compras.component';
import { FormTributosComponent } from './pages/form-tributos/form-tributos.component';
import { HomeComponent } from './pages/home/home.component';
import { InicioSesionComponent } from './pages/inicio-sesion/inicio-sesion.component';
import { RegistroCuentaComponent } from './pages/registro-cuenta/registro-cuenta.component';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/compat/auth-guard';
import { ViewFileComponent } from './pages/view-file/view-file.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ViewComprasComponent } from './pages/view-compras/view-compras.component';
import { FormContratoComponent } from './pages/form-contrato/form-contrato.component';
import { ViewContratoComponent } from './pages/view-contrato/view-contrato.component';
import { AuthGuard } from './pages/shared/guard/auth.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'tributos', component: FormTributosComponent },
  { path: 'compras', component: FormComprasComponent },
  { path: 'registro', component: RegistroCuentaComponent },
  { path: 'viewFile', component: ViewFileComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'viewCompra', component: ViewComprasComponent },
  { path: 'viewContrato', component: ViewContratoComponent },
  { path: 'formContrato', component:  FormContratoComponent},
  { path: 'login', component: InicioSesionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
