import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  messagingFirebase: firebase.messaging.Messaging;

  constructor() {
  }

  requestPermission = () => {
    return new Promise(async (resolve, reject) => {
      const permis = await Notification.requestPermission();
      if (permis === "granted") {
        const tokenFirebase = await this.messagingFirebase.getToken();
        resolve(tokenFirebase);
      } else {
        reject(new Error("No se otorgaron los permisos"))
      }

    })
  }

  private messagingOrservable = new Observable(observe => {
    this.messagingFirebase.onMessage(payload => {
      observe.next(payload);
    })
  })

  receiveMessage() {
    return this.messagingOrservable;
  }


}
