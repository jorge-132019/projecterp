import { TestBed } from '@angular/core/testing';

import { authControl } from './authControl';

describe('ChatService', () => {
  let service: authControl;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(authControl);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
