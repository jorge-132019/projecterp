import { Component } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { take } from 'rxjs/operators';
import firebase from 'firebase/compat/app';
import 'firebase/messaging';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent {
  constructor(private messaging: AngularFireMessaging) { }

  requestPermission() {
    this.messaging.requestPermission
      .pipe(take(1))
      .subscribe(
        (res) => {
          console.log('Permission granted!', res);
          this.messaging.getToken
            .pipe(take(1))
            .subscribe(
              (token) => {
                console.log('Token:', token);
                // Aquí podemos enviar el token a nuestro servidor para poder enviar notificaciones a este dispositivo
              },
              (err) => {
                console.error(err);
              }
            );
        },
        (err) => {
          console.error(err);
        }
      );
  }
}
