export interface GenericResponse<T> {
  status : number;
  data : T;
  errors : any;
}