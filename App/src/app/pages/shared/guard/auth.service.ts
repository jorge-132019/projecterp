import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userData$ = new BehaviorSubject<any>(null);

  constructor(private router: Router) {
    const userData = JSON.parse(localStorage.getItem('userData') ?? '{}');
    if (userData) {
      this.userData$.next(userData);
    }
  }

  isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    const lastActiveTime = localStorage.getItem('lastActiveTime');
    if (!token || !lastActiveTime) {
      return false;
    }
    const now = new Date();
    const lastActive = new Date(parseInt(lastActiveTime));
    const timeDiff = now.getTime() - lastActive.getTime();
    const dayInMillis = 24 * 60 * 60 * 1000;
    if (timeDiff >= dayInMillis) {
      localStorage.removeItem('token');
      localStorage.removeItem('lastActiveTime');
      return false;
    }
    const expiresIn = dayInMillis - timeDiff;
    console.log(`Token expira en ${expiresIn} milisegundos.`);

    return true;
  }

  setUserData(userData: any) {
    this.userData$.next(userData);
    localStorage.setItem('userData', JSON.stringify(userData));
    localStorage.setItem('lastActiveTime', new Date().getTime().toString());
  }

  getUserData() {
    return JSON.parse(localStorage.getItem('userData') ?? '{}');
  }

  get UserData$() {
    return this.userData$.asObservable();
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('lastActiveTime');
    this.router.navigate(['/login']);
  }
}
