export interface CreateTributeRequest {
  period : string;
  conceptCode : number;
  ConceptCodeName : string;
  tributeTypeCodeName: string;
  tributeTypeCode: number;
  FileRoute : string[];
  FileContent : string[];
  CreationUser : string;

}