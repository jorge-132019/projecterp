export interface TributeResponse {
  period : string;
  conceptCode : number;
  conceptCodeName : string;
  tributeTypeCodeName: string;
  tributeTypeCode: number;
  fileRoute : string[];
  link : string[];
  creationUser : string;
  creationDate : string;
}