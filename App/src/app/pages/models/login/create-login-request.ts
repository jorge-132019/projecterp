export interface CreateLoginRequest {
    email: string;
    password: string;
}