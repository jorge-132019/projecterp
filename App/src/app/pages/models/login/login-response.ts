export interface LoginResponse {
    email: string;
    lastNames: string;
    names: string;
    nickname: string;
    token: string;
}