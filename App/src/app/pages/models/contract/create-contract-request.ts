export interface CreateContractRequest {

  contractTypeCode: number;
  contractTypeCodeName: string;
  name: string;
  lastName: string;
  documentTypeCode: number;
  documentTypeCodeName: string;
  documentNumber: string;
  address: string;
  salary: number;
  startDate: string;
  endDate: string;
  jobPositionCode: number;
  jobPositionCodeName: string;
  fileRoute: string;
  creationUser: string;
}