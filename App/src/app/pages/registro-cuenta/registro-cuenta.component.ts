import { Component, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-registro-cuenta',
  templateUrl: './registro-cuenta.component.html',
  styleUrls: ['./registro-cuenta.component.css']
})
export class RegistroCuentaComponent {
  email: string;
  password: string;
  name: string;
  checkTermin: string;
  user: string;

  @ViewChild('formulario') formulario: NgForm;

  onSubmit() {

    if (!this.email || !this.password || !this.checkTermin || !this.user
      || !this.name) {
      Swal.fire({
        title: 'Campos vacíos',
        text: 'Debe de completar todos los campos',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      return;
    } else {
      Swal.fire({
        title: 'Te registraste exitosamente!',
        icon: 'success',
        confirmButtonText: 'Ok'
      });
    }

    console.log('Nombre:', this.name);
    console.log('Correo:', this.email);
    console.log('Usuario:', this.user);
    console.log('Contraseña:', this.password);
    console.log('Acept Termins:', this.checkTermin);

    this.limpiarCampos();
  }

  limpiarCampos() {
    this.formulario.reset();
  }

}
