import { Component,  ViewChild, ElementRef } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Observable } from 'rxjs';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-view-file',
  templateUrl: './view-file.component.html',
  styleUrls: ['./view-file.component.css']
})
export class ViewFileComponent {

  selectedFile: File;
  selectedDate: string;
  downloadURL: Observable<string>;
  files: any[];

  constructor(private storage: AngularFireStorage, private _CargarScripts: CargarScriptsService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  enviarHabilitado = false;
  selectConcept: string;

  validarCampos() {
    if (this.selectedDate && this.selectConcept) {
      this.enviarHabilitado = true;
    } else {
      this.enviarHabilitado = false;
    }
  }

  //Funcion click. visualizacion de pdf con un visor de pdfs
  viewPdf() {

    const selectedDate = new Date(this.selectedDate);
    const year = selectedDate.getFullYear();
    const month = selectedDate.toLocaleString('default', { month: 'long', timeZone: 'UTC' });

    const storagePath = `${year}/${month}`;

    const fileRef = this.storage.ref(`tributos/${storagePath}/TC02_GRUPO04.pdf`);
    fileRef.getDownloadURL().subscribe(url => {
      window.open(url);
    });
  }
}
