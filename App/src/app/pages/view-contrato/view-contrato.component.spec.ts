import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewContratoComponent } from './view-contrato.component';

describe('ViewContratoComponent', () => {
  let component: ViewContratoComponent;
  let fixture: ComponentFixture<ViewContratoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewContratoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
