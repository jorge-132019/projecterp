import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-view-contrato',
  templateUrl: './view-contrato.component.html',
  styleUrls: ['./view-contrato.component.css']
})
export class ViewContratoComponent {
  constructor(private _CargarScripts: CargarScriptsService) {
    _CargarScripts.Carga(["principal/main"]);
  }
}
