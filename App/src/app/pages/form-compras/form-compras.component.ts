import { Component } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { authControl } from 'src/app/providers/authControl';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-compras',
  templateUrl: './form-compras.component.html',
  styleUrls: ['./form-compras.component.css']
})
export class FormComprasComponent {
  constructor(private storage: AngularFireStorage, public _cs: authControl, private _CargarScripts: CargarScriptsService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  selectedFile: File;
  selectedDate: string;
  alertShown = false;

  currentYear = new Date().getFullYear();

  onFileSelected(event: any): void {
    this.selectedFile = event.target.files[0];
  }

  onSave(): void {

    if (!this.selectedFile || !this.selectedDate) {
      Swal.fire({
        title: 'Campos vacíos',
        text: 'Debe seleccionar un archivo y una fecha para guardarlo.',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      return;
    }

    const selectedDate = new Date(this.selectedDate);
    const year = selectedDate.getFullYear();

    const month = selectedDate.toLocaleString('default', { month: 'long', timeZone: 'UTC' });

    const storagePath = `${year}/${month}`;

    const fileRef = this.storage.ref(`compras/${storagePath}/${this.selectedFile.name}`);
    const task = this.storage.upload(`compras/${storagePath}/${this.selectedFile.name}`, this.selectedFile);

    //Validamos si el archivo subido tiene la extension .pdf
    if (this.selectedFile.type !== 'application/pdf') {
      Swal.fire({
        title: 'Archivo Incorrecto',
        text: 'Seleccione un archivo .pdf',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      return;
    }

    task.snapshotChanges().subscribe(() => {
      if (!this.alertShown) {
        Swal.fire({
          title: 'Archivo subido',
          text: 'El archivo se ha subido exitosamente.',
          icon: 'success',
          confirmButtonText: 'Ok'
        });
        this.alertShown = true;
      }
      this.selectedDate = '';
    });
  }

  saveData(form: any) {
    console.log("Enviando Datos");
    console.log(form);

  }
}
