import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import '@firebase/auth';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { CreateLoginRequest } from '../models/login/create-login-request';
import { ToastrService } from 'ngx-toastr';
import { DataState } from '../shared/enum/data-state';
import { AppState } from '../shared/app-state';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { GenericResponse } from '../shared/generic-response';
import { InicioSesionService } from './inicio-sesion.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '../shared/guard/auth.service';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.css']
})
export class InicioSesionComponent implements OnInit {

  @ViewChild('formulario') formulario: NgForm;

  constructor(private afAuth: AngularFireAuth, private router: Router,
    private loginService: InicioSesionService, private toastr: ToastrService,
    private authService: AuthService) { }

  email: string;
  password: string;
  showPass = false;
  appState$ = new Observable<AppState<GenericResponse<any>>>();
  errroMessage: string;
  private readonly dataState = DataState;
  // @ts-ignore
  private dataSubject = new BehaviorSubject<GenericResponse<any>>(null);

  private isLoading = new BehaviorSubject<Boolean>(false);

  isloading$ = this.isLoading.asObservable();

  showPassword(): void {
    this.showPass = !this.showPass;
    const passwordInput = document.getElementById('yourPassword') as HTMLInputElement;
    passwordInput.type = this.showPass ? 'text' : 'password';
  }

  ngOnInit(): void {
    //this.afAuth.authState.subscribe(user => {
    //if (user) {
    //this.router.navigate(['home']);
    //}
    //});

    window.addEventListener('popstate', () => {
      if (!this.authService.isLoggedIn()) {
        this.router.navigate(['/login']);
      }
    });
  }

  signInWithGoogle() {
    this.afAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  private showSuccessToast(title: string, message: string): void {
    this.toastr.success(message, title, {
      timeOut: 4000,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true
    })
  }

  private showErrorToast(title: string, message: string): void {
    this.toastr.error(message, title, {
      progressBar: true,
      closeButton: true,
      timeOut: 4000,
      positionClass: 'toast-top-right'
    })
  }

  encryptInfo(cipher: string): string {
    return CryptoJS.AES.encrypt(
      cipher,
      CryptoJS.enc.Utf8.parse(environment.PrivateKeyEncryption),
      {
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
        iv: CryptoJS.enc.Hex.parse('00000000000000000000000000000000'),
      }
    ).toString();
  }

  async onSubmit(): Promise<void> {

    let login: CreateLoginRequest = {
      password: this.encryptInfo(this.password),
      email: this.encryptInfo(this.email)
    }

    console.log("Email encryption: ", this.encryptInfo(this.email));
    console.log("Password encryption: ", this.encryptInfo(this.password),);

    this.isLoading.next(true);
    this.appState$ = this.loginService.createLoginRequest$(login).pipe(
      map((res) => {
        this.isLoading.next(false);
        if (res.status !== 200) this.showErrorToast("ERROR", "Lo sentimos, hubo un problema al completar el registro debido a un error inesperado.");
        else {
          const now = new Date();
          const expiresAt = new Date(now.getTime() + (24 * 60 * 60 * 1000)); // Set the token expiration time to 1 day from now
          const tokenData = {
            token: res.data.token,
            expiresAt: expiresAt.getTime() // Save the expiration time as a timestamp
          };
          localStorage.setItem('token', res.data.token);
          localStorage.setItem('lastActiveTime', now.getTime().toString());
          if (this.authService.isLoggedIn()) {
            this.showSuccessToast("Registro exitoso", "El acceso al inicio de sesion ha sido registrado exitosamente.");
            const userData = {
              name: res.data.names,
              lastNames: res.data.lastNames,
              nickname: res.data.nickname,
              email: res.data.email,
              UID: res.data.token,
            };
            this.authService.setUserData(userData);
            localStorage.setItem('token', res.data.token);
            this.router.navigate(['/home']);
          } else {
            this.router.navigate(['/login']);
          }
        }
        return { dataState: this.dataState.LOADED_STATE, data: res }
      })
    );

    if (!this.email || !this.password) {
      Swal.fire({
        title: 'Campos vacíos',
        text: 'Debe de completar todos los campos',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      return;
    }

    this.limpiarCampos();
  }

  limpiarCampos() {
    this.formulario.reset();
  }
}
