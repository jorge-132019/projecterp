import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GenericResponse } from '../shared/generic-response';
import { CreateLoginRequest } from '../models/login/create-login-request';
import { LoginResponse } from '../models/login/login-response';

@Injectable({
  providedIn: 'root'
})

export class InicioSesionService {

  private readonly apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  createLoginRequest$ = (request: CreateLoginRequest) => <Observable<GenericResponse<LoginResponse>>>this.http.post<GenericResponse<LoginResponse>>(`${this.apiUrl}/login`, request, {
    headers: {
    },
  });
}
