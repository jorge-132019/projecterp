import { TestBed } from '@angular/core/testing';

import { TributeService } from './tribute.service';

describe('TributeService', () => {
  let service: TributeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TributeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
