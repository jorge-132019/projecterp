import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTributosComponent } from './form-tributos.component';

describe('FormTributosComponent', () => {
  let component: FormTributosComponent;
  let fixture: ComponentFixture<FormTributosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTributosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormTributosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
