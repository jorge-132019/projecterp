import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenericResponse } from '../shared/generic-response';
import { TributeResponse } from '../models/tribute/tribute-response';
import { CreateTributeRequest } from '../models/tribute/create-tribute-request';

@Injectable({
  providedIn: 'root'
})
export class TributeService {

  private readonly apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  createTributeRequest$ = (request: CreateTributeRequest) => <Observable<GenericResponse<TributeResponse>>>this.http.post<GenericResponse<TributeResponse>>(`${this.apiUrl}/tribute`, request, {
    headers: {
      Authorization: `Bearer ${environment.authorizationToken}`,
    },
  });

}
