import { Component } from '@angular/core';
import { authControl } from 'src/app/providers/authControl';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';
import { finalize, BehaviorSubject, map, Observable, startWith } from 'rxjs';
import { TributeService } from './tribute.service';
import { DataState } from '../shared/enum/data-state';
import { AppState } from '../shared/app-state';
import { CreateTributeRequest } from '../models/tribute/create-tribute-request';
import { TributeResponse } from '../models/tribute/tribute-response';
import { GenericResponse } from '../shared/generic-response';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../shared/guard/auth.service';

@Component({
  selector: 'app-form-tributos',
  templateUrl: './form-tributos.component.html',
  styleUrls: ['./form-tributos.component.css']
})
export class FormTributosComponent {

  public isConceptDisabled = false;
  public isMultipleFilesAllowed = false;
  selectedFile: FileList;
  selectedDate: string;
  selectConcept: string;
  selectTypeTribute: string;
  currentYear = new Date().getFullYear();
  conceptContent = { "conceptValue": 0, "conceptText": "" };
  tributeContent = { "tributeValue": 0, "tributeText": "" };
  appState$ = new Observable<AppState<GenericResponse<any>>>();
  errroMessage: string;
  private fileContent: string[] = [];
  private fileRoute: string[] = [];
  private readonly dataState = DataState;
  public userData: any = {};
  // @ts-ignore
  private dataSubject = new BehaviorSubject<GenericResponse<any>>(null);

  private isLoading = new BehaviorSubject<Boolean>(false);

  isloading$ = this.isLoading.asObservable();

  constructor(public _cs: authControl, private _CargarScripts: CargarScriptsService, private tributeService: TributeService, private toastr: ToastrService, private authService: AuthService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  ngOnInit() {
    this.userData = this.authService.getUserData();
  }

  private convertFileToBase64(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const result = reader.result?.toString().split(',')[1];
        if (result) {
          resolve(result);
        } else {
          reject('Error al leer el archivo');
        }
      };
      reader.onerror = (error) => reject(error);
    });
  }

  onConceptSelected(event: any): void {
    this.conceptContent = { "conceptValue": event.target.value, "conceptText": event.target.selectedOptions[0].text };
  }

  onTypeTributeSelected(event: any): void {
    this.tributeContent = { "tributeValue": event.target.value, "tributeText": event.target.selectedOptions[0].text };
    if (event.target.value === "0") {
      this.isMultipleFilesAllowed = false;
      this.isConceptDisabled = false;

    } else if (event.target.value === "1") {
      this.isMultipleFilesAllowed = true;
      this.isConceptDisabled = true;
    }
  }

  // Agrega esta linea para habilitar el select de concepto al inicio
  if(isConceptDisabled: any) {
    this.isConceptDisabled = false;
  }

  onFileSelected(event: any): void {
    this.selectedFile = event.target.files;

    if (!this.isMultipleFilesAllowed && this.selectedFile.length > 1) {
      this.errroMessage = "Solo se puede seleccionar un archivo en tributos mensuales.";
      return;
    }
  }

  private showSuccessToast(title: string, message: string): void {
    this.toastr.success(message, title, {
      timeOut: 4000,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true
    })
  }

  private showWarningToast(title: string, message: string): void {
    this.toastr.warning(message, title, {
      timeOut: 4000,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true
    })
  }

  private showErrorToast(title: string, message: string): void {
    this.toastr.error(message, title, {
      progressBar: true,
      closeButton: true,
      timeOut: 4000,
      positionClass: 'toast-top-right'
    })
  }

  private ValidateFileFormart(): Boolean {
    this.errroMessage = "";
    for (let i = 0; i < this.selectedFile.length; i++) {
      if (this.selectedFile[i].type !== 'application/pdf') {
        this.errroMessage = "No cumple con el formato (.pdf)";
        return false;
      }
    }
    return true;
  }

  private ValidateInputs(): boolean {
    if (this.selectedFile && this.selectedDate && (this.isConceptDisabled || this.selectConcept)) {
      return true;
    } else {
      this.showWarningToast("Campos Requeridos", "Todos los campos son requeridos.");
      return false;
    }
  }


  async onSave(): Promise<void> {
    if (this.ValidateInputs() && this.ValidateFileFormart()) {
      const [year, month] = this.selectedDate.split('-');

      const storagePath = `${month}-${year}`;
      const yearFromPath = storagePath.split('-')[1];
      console.log(yearFromPath); // imprime el año en la consola


      if (this.selectTypeTribute === '1') {
        for (let index = 0; index < this.selectedFile.length; index++) {
          let file = this.selectedFile[index];
          this.fileContent[index] = await this.convertFileToBase64(file);
          this.fileRoute[index] = `tributos/${yearFromPath}/${this.tributeContent.tributeText}/${file.name}`;
        }
      } else {
        let file = this.selectedFile[0];
        this.fileContent[0] = await this.convertFileToBase64(file);
        this.fileRoute[0] = `tributos/${yearFromPath}/${this.tributeContent.tributeText}/${storagePath}/${file.name}`;
      }

      let tribute: CreateTributeRequest = {
        period: `${month}/${year}`,
        conceptCode: parseInt(this.conceptContent.conceptValue.toString()),
        ConceptCodeName: this.conceptContent.conceptText,
        tributeTypeCode: parseInt(this.tributeContent.tributeValue.toString()),
        tributeTypeCodeName: this.tributeContent.tributeText,
        FileRoute: this.fileRoute,
        FileContent: this.fileContent,
        CreationUser: this.userData.email,
      }

      console.log(tribute);
      this.isLoading.next(true);
      this.appState$ = this.tributeService.createTributeRequest$(tribute).pipe(
        map((res) => {
          //this.dataSubject.next(res);
          this.isLoading.next(false);
          if (res.status !== 200) this.showErrorToast("ERROR", "Lo sentimos, hubo un problema al completar el registro debido a un error inesperado.");
          else this.showSuccessToast("Registro exitoso", "El tributo ha sido registrado exitosamente.");
          console.log("Tributo Registrado.", res.data);

          return { dataState: this.dataState.LOADED_STATE, data: res }
        })
      );

    }

  }
}
