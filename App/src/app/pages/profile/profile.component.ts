import { Component, OnInit } from '@angular/core';
import { authControl } from 'src/app/providers/authControl';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';
import { AuthService } from '../shared/guard/auth.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  constructor(public _cs: authControl, private _CargarScripts: CargarScriptsService, private authService: AuthService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  public userData: any = {};

  ngOnInit() {
    this.userData = this.authService.getUserData();
  }

  getInitial(name: string): string {
    if (!name) {
      return '';
    }
    const initials = name.charAt(0).toUpperCase();
    return `https://eu.ui-avatars.com/api/?name=${initials}&background=FF5722&color=fff&size=30`;
  }
}
