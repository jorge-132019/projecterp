import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-view-compras',
  templateUrl: './view-compras.component.html',
  styleUrls: ['./view-compras.component.css']
})
export class ViewComprasComponent {
  constructor(private _CargarScripts: CargarScriptsService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  enviarHabilitado = false;
  selectConcept: string;
  selectedRazon: string;
  selectedDate: string;

  validarCampos() {
    if (this.selectedDate && this.selectConcept &&this.selectedRazon) {
      this.enviarHabilitado = true;
    } else {
      this.enviarHabilitado = false;
    }
  }

}
