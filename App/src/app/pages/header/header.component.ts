import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { authControl } from 'src/app/providers/authControl';
import { AuthService } from '../shared/guard/auth.service';
import { LoginResponse } from '../models/login/login-response';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userData: any = {};

  constructor(public _cs: authControl, private afAuth: AngularFireAuth, private router: Router,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.userData = this.authService.getUserData();
  }

  logout() {
    this.authService.logout();
  }

  getInitial(name: string): string {
    if (!name) {
      return '';
    }
    const initials = name.charAt(0).toUpperCase();
    return `https://eu.ui-avatars.com/api/?name=${initials}&background=FF5722&color=fff&size=30`;
  }
}
