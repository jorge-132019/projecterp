import { Component, ViewChild } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';
import { NgForm } from '@angular/forms';
import { ContratoService } from './form-contrato.service';
import { CreateContractRequest } from '../models/contract/create-contract-request';
import { authControl } from 'src/app/providers/authControl';
import { ToastrService } from 'ngx-toastr';
import { Observable, BehaviorSubject, map } from 'rxjs'
import { AppState } from '../shared/app-state';
import { GenericResponse } from '../shared/generic-response';
import { DataState } from '../shared/enum/data-state';
import { AuthService } from '../shared/guard/auth.service';

@Component({
  selector: 'app-form-contrato',
  templateUrl: './form-contrato.component.html',
  styleUrls: ['./form-contrato.component.css']
})
export class FormContratoComponent {
  @ViewChild('formulario') formulario: NgForm;

  name: string;
  selectContrato: string;
  lastName: string;
  selectDocument: string;
  nroDocument: string;
  direccion: string;
  salary: number;
  dateInicio: string;
  dateFinal: string;
  selectCargo: string;
  errorMessage: string;
  public userData: any = {};

  appState$ = new Observable<AppState<GenericResponse<any>>>();
  private readonly dataState = DataState;

  private isLoading = new BehaviorSubject<Boolean>(false);
  isloading$ = this.isLoading.asObservable();

  private contractContent = { "contractValue": 0, "ContractText": "" };
  private jobPositionContent = { "jobValue": 0, "jobText": "" };
  private documentTypeContent = { "documentTypeValue": 0, "documentTypeText": "" };

  constructor(private _CargarScripts: CargarScriptsService, private contratoService: ContratoService, public _cs: authControl, private toastr: ToastrService,
    private authService: AuthService) {
    _CargarScripts.Carga(["principal/main"]);
  }

  ngOnInit() {
    this.userData = this.authService.getUserData();
  }

  private formatDate(selectedDate: string): string {
    const [year, month, day] = selectedDate.split('-');
    return `${day}/${month}/${year}`;
  }

  onContractSelected(event: any): any {
    this.contractContent = { "contractValue": event.target.value, "ContractText": event.target.selectedOptions[0].text };
  }

  onJobPositionSelected(event: any): void {
    this.jobPositionContent = { "jobValue": event.target.value, "jobText": event.target.selectedOptions[0].text }; +
      console.log("VALOR: ", this.jobPositionContent);
  }

  onDocumentTypeSelected(event: any): void {
    this.documentTypeContent = { "documentTypeValue": event.target.value, "documentTypeText": event.target.selectedOptions[0].text };
  }

  showSuccessToast() {
    this.toastr.success('El contrato se ha generado correctamente', 'Éxito', {
      timeOut: 4000,
      progressBar: true,
      closeButton: true,
      progressAnimation: 'increasing'
    });
  }

  showWarningToast() {
    this.toastr.warning('Completar todos los campos', 'Advertencia', {
      timeOut: 4000,
      progressBar: true,
      closeButton: true,
      progressAnimation: 'increasing'
    });
  }

  showErrorToast() {
    this.toastr.error('Error al guardar los datos', 'Error', {
      progressBar: true,
      closeButton: true,
      timeOut: 4000,
      progressAnimation: 'increasing',
    });
  }
  private ValidateDocumentNumber(): Boolean {
    this.errorMessage = '';
    if (this.selectDocument === '0') {
      if (/^[0-9]{8}$/.test(this.nroDocument)) return true;
      this.errorMessage = 'Por favor, ingrese un número de DNI válido (8 dígitos numéricos)';
    }
    else {
      if (/^[0-9]{11}$/.test(this.nroDocument)) return true;
      this.errorMessage = 'Por favor, ingrese un número de Carnet de extranjería válido (11 dígitos numéricos)';
    }
    return false;
  }

  private ValidateInputs(): Boolean {
    if (this.selectContrato && this.name && this.lastName && this.selectDocument && this.direccion && this.dateInicio && this.dateFinal && this.selectCargo) return true;
    else {
      this.showWarningToast();
      return false;
    }
  }

  onSubmit() {
    this.ValidateDocumentNumber();
    if (this.ValidateDocumentNumber() && this.ValidateInputs()) {
      let request: CreateContractRequest = {
        contractTypeCode: parseInt(this.contractContent.contractValue.toString()),
        contractTypeCodeName: this.contractContent.ContractText,
        name: this.name,
        lastName: this.lastName,
        documentTypeCode: parseInt(this.documentTypeContent.documentTypeValue.toString()),
        documentTypeCodeName: this.documentTypeContent.documentTypeText,
        documentNumber: this.nroDocument,
        address: this.direccion,
        salary: this.salary,
        startDate: this.formatDate(this.dateInicio),
        endDate: this.formatDate(this.dateFinal),
        jobPositionCode: parseInt(this.jobPositionContent.jobValue.toString()),
        jobPositionCodeName: this.jobPositionContent.jobText,
        fileRoute: `contratos/${this.nroDocument}/${this.contractContent.ContractText}/${this.dateInicio.replace('/', '-')}_${this.dateFinal.replace('/', '-')} .pdf`,
        creationUser: this.userData.email,
      }
      console.log(request);
      this.isLoading.next(true);
      this.appState$ = this.contratoService.createContract$(request).pipe(
        map((response) => {
          this.isLoading.next(false);
          if (response.status !== 200) this.showErrorToast();
          else this.showSuccessToast();
          console.log(response);
          return { dataState: this.dataState.LOADED_STATE, data: response }
        })
      );
      this.limpiarCampos();
    }
  }

  limpiarCampos() {
    this.formulario.reset();
    this.selectCargo = '';
    this.selectContrato = '';
    this.selectDocument = '';
  }
}
