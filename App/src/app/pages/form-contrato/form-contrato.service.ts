import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { CreateContractRequest } from '../models/contract/create-contract-request';
import { Observable } from 'rxjs';
import { GenericResponse } from '../shared/generic-response';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  private readonly apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  createContract$ = (request: CreateContractRequest) => <Observable<GenericResponse<any>>>this.http.post<GenericResponse<any>>(`${this.apiUrl}/contract`, request, {
    headers: {
      Authorization: `Bearer ${environment.authorizationToken}`,
    },
  });
}
