import { Component } from '@angular/core';
import { authControl } from './providers/authControl';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public _cs: authControl) {
  }
}
