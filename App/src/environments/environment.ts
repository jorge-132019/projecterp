export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCtJ8F34LdxejPTQe9hTJUtyIrKW2shmEA",
    authDomain: "firechat-f324e.firebaseapp.com",
    databaseURL: "https://firechat-f324e-default-rtdb.firebaseio.com",
    projectId: "firechat-f324e",
    storageBucket: "firechat-f324e.appspot.com",
    messagingSenderId: "428821316874",
    appId: "1:428821316874:web:faa4476c938084a50f66f3",
    measurementId: "G-QXFSNXRB2L"
  },

  authorizationToken: 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IklUT2IzdTdJanM1M3BsNVZSa2hpIiwibmJmIjoxNjgyNDcxMTI3LCJleHAiOjE2ODI1NTc1MjcsImlhdCI6MTY4MjQ3MTEyNywiaXNzIjoiYmFGazdkSWg2VjVYZW9qeWxUUm0iLCJhdWQiOiJOSnU2ejJnVTY3T3M5aXFUVnZ5ZyJ9.RZ64KUY6i_36bsIkFnp1d9rDu9Tn7NMvKNkbGAge69h2yKkj52mWyxU5yt_ZqVPTF69thYXlGBGUCOXK7ZF3kQ',

  apiUrl: 'https://localhost:7170/api',

  PrivateKeyEncryption: 'a2b1c8d4e5f69a4be4cf2a1d8e29b0c9',

};
